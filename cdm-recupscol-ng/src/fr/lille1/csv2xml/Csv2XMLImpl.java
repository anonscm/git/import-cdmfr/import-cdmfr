package fr.lille1.csv2xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.io.IOUtils;
import org.apache.commons.codec.binary.Hex;

public class Csv2XMLImpl implements Csv2XML{
	protected String model, root,  fileIn;
	protected char separator;
	protected InputStream inputStream;
	protected PrintStream printStream;

	protected String[] commandes={"help", "template", "root", "input", "output", "separator"};
	public Csv2XMLImpl(){
		model="template.xml";
		root="root";
		//separator='\u2021';
    separator=';';
		inputStream=System.in;
		printStream=System.out;
	}

	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getFileIn() {
		return fileIn;
	}

	public void setFileIn(String fileIn) {
		try {
			this.fileIn = fileIn;
			inputStream=new FileInputStream(new File(fileIn));
		} catch(Exception e) {
			error(-10,fileIn);
		}
	}
	public  String getArgName(String _arg){
		String name="";
		if(_arg.startsWith("--")) {
			String arg=_arg.substring(2);
			try {
				name=arg.split("=")[0];
			} catch(Exception e) {
				
			}
		}
		return name;
	}
	public  String getArgValue(String _arg){
		String value="";
		if(_arg.startsWith("--")) {
			String arg=_arg;
			try {
				value=arg.split("=")[1];
			} catch(Exception e) {
				
			}
		}
		return value;
	}
	public  void help(){
		System.out.println("Csv2Xml --template=template.xml [--input=in.csv] [--output=out.csv]");
	}
	private boolean verifyModel(){
		File file=new File(model);
		return file.exists();
	}

	public void setFileOut(String fileOut){
		try {
			printStream=new PrintStream(fileOut, "UTF-8");
		} catch(Exception e) {
			error(-10,fileOut);
		}
	}
	private boolean isFileIn() {
		return fileIn.length()>0;
	}
	public List<CSVRecord> getRecords() {
		try {
			System.out.println(this);
			return CSVFormat.EXCEL.withDelimiter(getSeparator()).parse(new InputStreamReader(inputStream)).getRecords();
		} catch (IOException e) {
			error(-10,e.getMessage());
		}
		return new ArrayList<CSVRecord>();
	}
	public String getModelString(){
    FileInputStream fisTargetFile =null;
		try {
			fisTargetFile = new FileInputStream(new File(model));
			return IOUtils.toString(fisTargetFile, "UTF-8");
		} catch (Exception e) {
      e.printStackTrace();
			error(-10, e.getMessage());
		} finally {
      try{fisTargetFile.close();}catch(IOException e){}
    }
		return "";
	}
  public String getEntites(){
    return null;
	}
	public void outputStarts(){
	try {	
		printStream.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    if(getEntites!=null&&getEntites().length()>0) printStream.println(getEntites());
		printStream.println("<"+root+">");
	} catch (Exception e) {
		error(-10, e.getMessage());
	}
	}
	public void output(String s){
		try {	
			printStream.println(s);
		} catch (Exception e) {
			error(-10, e.getMessage());
		}
		}
	public void outputEnds(){
		try {	
			printStream.println("</"+root+">");
		} catch (Exception e) {
			error(-10, e.getMessage());
		}
		}
	private boolean verifyFileIn() {
		if(fileIn.length()>0){
			return (new File(fileIn).exists());
		}
		return true ;
	}
	public void setSeparator(char separator){
		this.separator=separator;
	}
	public void execute() {
		try {
			if(verifyModel()) {
				if(verifyFileIn()){		
					outputStarts();
					for(CSVRecord record : getRecords()) {
						String modelString=getModelString();
					    for(int i=0; i<record.size(); i++){
					    	modelString=modelString.replaceAll("##"+i+"##", encodeXML(record.get(i)));
					    	modelString=modelString.replaceAll("#!"+i+"!#", transformXML(record.get(i)));//texte riche rimbaus
					    }
					    output(modelString);
					}
					outputEnds();
				} else error(-3,getFileIn());
				
			} else error(-1,getModel());
		} catch (Exception e) {
      e.printStackTrace();
			error(-10,e.getMessage());
		}
	}
	public String toString(){
		return "model="+getModel()+" root="+getRoot()+ " separator="+getSeparator();
	}
	public void error(int i, String arg){
		switch (i){
			case -1 : System.err.println("le fichier du modele de sortie n'existe pas ("+arg+")"); break;
			case -2 : System.err.println("un argument de la commande est inconnu : "+arg);  break;
			case -3 : System.err.println("Le fichier d'entree n'existe pas ("+arg+")");  break;
			case -10 : System.err.println("Erreur d'execution ("+arg+")");  break;
		}
		System.exit(i);
	}
	public char getSeparator() {
		return separator;
	}

	public  Csv2XML getCsv2XML(String[] args){
		for(String _arg:args){
			String arg=getArgName(_arg);
			int argi=getIndex(arg);
			String value=getArgValue(_arg);
      System.out.println(_arg+" "+arg+"("+argi+")="+value);
			switch (argi) {
				case 0 :  help(); System.exit(0);
				case 1 : setModel(value); break;
				case 2 : setRoot(value); break;
				case 3 : setFileIn(value); break;
				case 4 : setFileOut(value); break;
				case 5 : if(value.length()==1)setSeparator(value.charAt(0));test(String.valueOf(getSeparator())); break;
				default :  break;
			}	
		}
		return this;
	}
  private void test(String arg){
    System.out.println("Separateur utilise (code en hexadecimal) :" + Hex.encodeHexString(arg.getBytes(/* charset */)));
  }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Csv2XML csv2XML=new Csv2XMLImpl().getCsv2XML(args);
		System.out.println(csv2XML);
    System.out.println("-----------------------");
		csv2XML.execute();
    System.out.println("-----------------------");
		System.out.println(csv2XML);
	}
	private String encodeXML(String s){
		return s.replaceAll("&", "&amp;").replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("\"","&quot;").replaceAll("'","&apos;");
	}
	public String transformXML(String s){
		  return s;

	}
	public int getIndex(String s){
		int i=0;
		for(String c : commandes){
			if(c.equals(s)) return i;
			i++;
		}
		return -1;
	}

}
