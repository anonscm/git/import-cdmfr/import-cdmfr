package fr.lille1.csv2xml;


import java.util.List;

import org.apache.commons.csv.CSVRecord;


public interface Csv2XML {



	public String getModel() ;
	public void setModel(String model);
	public String getRoot();
	public void setRoot(String root);
	public String getFileIn();

	public void setFileIn(String fileIn);
	public String getArgName(String _arg);
	public String getArgValue(String _arg);
	public void help();
	public void error(int i, String arg);

	public void setFileOut(String fileOut);
	public List<CSVRecord> getRecords();
	public String getModelString();
  public String getEntites();
	public void outputStarts();
	public void output(String s);
	public void outputEnds();

	public void setSeparator(char separator);
  public char getSeparator();
	public void execute() ;
	public String toString();
	public  Csv2XML getCsv2XML(String[] args);

	public String transformXML(String s);

}
