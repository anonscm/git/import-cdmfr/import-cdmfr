package fr.lille1.csv2xml;

import org.apache.commons.io.FileUtils;
import java.io.PrintStream;

public class GiseleCsv2XmlImpl extends Csv2XMLImpl {
  public GiseleCsv2XmlImpl(){
    super();
    separator=0x87;
  }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Csv2XML giseleCsv2Xml=new GiseleCsv2XmlImpl().getCsv2XML(args);
		System.out.println("-----------------------");
    System.out.println("file.encoding="+System.getProperty("file.encoding"));
    System.out.println("-----------------------");
		giseleCsv2Xml.execute();
    System.out.println("-----------------------");
	}
	public void help(){
		System.out.println("java -jar GiseleCsv2XmlImpl.jar --model=template.xml [--root=main] [--input=in.csv] [--output=out.csv] [--separator=;]");
	}
	public String transformXML(String s){
		  return s.replaceAll("~","<br/>").replaceAll("<!--.*-->","").replaceAll("<br>","<br/>").replaceAll("<I[^>]*>","<i>").replaceAll("</I[^>]*>","</i>").replaceAll("</UL[^>]*>","</ul>").replaceAll("</LI[^>]*>","</li>").replaceAll("<UL[^>]*>","<ul>").replaceAll("<LI[^>]*>","<li>").replaceAll("<BR[^>]*>","<br/>").replaceAll("<B[^>R]*>","<b>").replaceAll("</B[^>R]*>","</b>").replaceAll("<FONT[^>]*>","").replaceAll("</FONT>","").replaceAll("&nbsp;"," ").replaceAll("&([^a][^m][^p][^;])","&amp;$1").replaceAll("<img ([^>]*)>","<img $1/>").replaceAll("<((?![a-zA-Z]([ \t]+[a-zA-Z][^ \t]*=(\".*\"|'.*'|[^ \t]*))*|/[a-zA-Z]+[ \t]*>))","&lt;$1");

	}
  public void setSeparator(char separator){
	}
  public void getEntites(){
    try {	
      return FileUtils.readFileToString(new File("xsl/entites.ent"));
    } catch (Exception e) {
      error(-10, e.getMessage());
    }
    return null;
	}
}
