package fr.lille1.uniform ; 
  
import java.io.File ;
import java.io.IOException ;
import java.io.FileNotFoundException ;
import java.io.FileOutputStream ;
import java.io.FileInputStream ;
import java.io.StringWriter ;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import javax.xml.parsers.DocumentBuilder ;
import javax.xml.parsers.DocumentBuilderFactory ;

import org.w3c.dom.*;


import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import org.apache.xpath.XPathAPI ;
import fr.lille1.util.XslTransformer ;

public class ImportCDM {
    

  private String uid="";
  private String password="";
  private String annee="2014";
  private String url="http://formations-encours.univ-lille1.fr/cdm/services/sof";
  private String source="cdm-content/catalogue-local.xml";
  private String xsl="xsl/catalogue-cdm.xsl";
  private String commande="storeCDM";
  private String outputDir="sortie";
  protected static String[] arguments={"help", "uid", "password", "annee", "url", "source", "xsl", "commande", "outputDir"};
  protected static String[] commandes={"storeCDM", "setOrgUnit", "setConstants"};

	public ImportCDM ()  {
	} 

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getXsl() {
		return xsl;
	}

	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	public String getCommande() {
		return commande;
	}

	public void setCommande(String commande) {
		this.commande = commande;
	}
  
  public String getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}
	
	private void saveDocument(String filename, Document doc) throws Exception  {
		File cdmFile = new File(filename) ;
		Transformer xmlCopy = xmlCopy = XslTransformer.getTransformer();
		//xmlCopy.setOutputProperty(OutputKeys.ENCODING, System.getProperty("file.encoding"));
    xmlCopy.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		FileOutputStream fileOutputStream = new FileOutputStream(cdmFile);
		StreamResult streamResult = new StreamResult(fileOutputStream);
		DOMSource domSource = new DOMSource(doc);
		xmlCopy.transform(domSource, streamResult);
		cdmFile.createNewFile();
		fileOutputStream.close();
	}


  	private Document getDocument(String filename) throws Exception {
      return getDocumentBuilder().parse(new File(filename)) ;
  	}

  	private DocumentBuilder getDocumentBuilder() throws Exception {
  		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true); 
      return dbf.newDocumentBuilder();	
  	}
    public void importMentions() {
    try {
      for(String var : getMentions()){
        System.out.println("var="+var);
        importMention(var);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
   }
  	public boolean importMention(String var) throws Exception {
		try {
      System.out.println("xsl="+getXsl());
			Transformer mention = XslTransformer.getTransformer(getXsl()) ;
			Document doc = getDocument(getSource()) ;
			DOMResult resDoc = new DOMResult() ;
			mention.setParameter("code", var) ;
			mention.transform(new DOMSource(doc), resDoc) ;
			saveDocument(getOutputFile(var), (Document)resDoc.getNode() ) ;
			return sendDocumentToUniform((Document)resDoc.getNode()) ;
		} catch (Exception e) {
			e.printStackTrace() ;
		}
		return false ;
  	}



   /*
    *
    *
    *
    * OK
    *
    */
   private boolean sendDocumentToUniform(Document  doc) throws Exception {
	   try {
	   		if(isUrlValid()){
          Service  service = new Service();
          Call     call    = (Call) service.createCall();
          call.setTargetEndpointAddress( new java.net.URL(url) );
          call.setOperationName("storeCDM");
          StringWriter xmlString = new StringWriter();
          Transformer trans=XslTransformer.getTransformer();
          StreamResult res=new StreamResult(xmlString);          
          DOMSource src=new DOMSource(doc);
          trans.transform(src,res);
          Object[] objects=new Object[5];
          objects[0]=uid;
          objects[1]=password;
          objects[2]= annee ;
          objects[3]= getId(doc, "/CDM/program[1]/@id") ;
          objects[4]=xmlString.toString();
          String str=(String) call.invoke(objects);  
        }
        return true ;
		} catch (Exception e) {
			e.printStackTrace() ;
			System.out.println("ws exception: "+e) ;
			return false ;
		}
		   
	   	
   } 
    private String getOutputFile(String var){
      return getOutputDir()+"/"+var+".xml";
    }
   private boolean sendConstantsToUniform(Document  doc) throws Exception {
		 return sendDataToUniform("setConstants", doc) ; 
   }
   private boolean sendOrgUnitToUniform(Document  doc) throws Exception {
		return sendDataToUniform("setOrgUnit", doc) ; 
   }
   private boolean isUrlValid(){
      return getUrl().startsWith("http:")||getUrl().startsWith("https:");
   }
         /*
    *
    *
    *
    * OK
    *
    */
   private boolean sendDataToUniform(String cmd, Document  doc) throws Exception {
	   try {
	   		if(isUrlValid()){
          Service  service = new Service();
          Call     call    = (Call) service.createCall();
          call.setTargetEndpointAddress( new java.net.URL(url) );
          call.setOperationName(cmd);
          StringWriter xmlString = new StringWriter();
          Transformer trans=XslTransformer.getTransformer();
          StreamResult res=new StreamResult(xmlString);        
          DOMSource src=new DOMSource(doc);
          trans.transform(src,res);
          Object[] objects=new Object[3];
          objects[0]=uid; 
          objects[1]=password; 
          objects[2]=xmlString.toString();
          //String str=(String) call.invoke(objects);  
          System.out.println("envoi="+xmlString) ;
        }
        return true ;
		} catch (Exception e) {
        e.printStackTrace() ;
        System.out.println("ws exception: "+e) ;
        return false ;
		}
   }
   
   /*
    *
    *
    *
    * OK
    *
    */
   private String getId(Document doc, String xpath) throws Exception  {
   		return getNodeValues(doc, xpath)[0] ;
   }
   
   private String[] getNodeValues(Document doc, String xpath) throws Exception  {
		NodeList nl = XPathAPI.selectNodeList(doc, xpath) ;
		String[] result = new String[nl.getLength()] ;
		for(int i = 0 ; i < result.length ; i++) 
			result[i] = nl.item(i).getNodeValue() ;
		return result ;
   }
   
   public String[] getMentions() throws Exception  {
   		return getNodeValues(getDocument(source), "/CDM-complet/program[programDescription/@nature='mention']/@id") ;
   }  
  

  public static void main(String[] args) {
	  ImportCDM importCDM=getImportCDM(args);
	  importCDM.execute();
 }
  private void execute(){
    switch (getCommandeIndex()) {
				case 0 : importMentions(); break;
				case 1 :  break;
				case 2 :  break;

				default :  break;
			}	
  }
  private int getCommandeIndex(){
    int i=0;
		for(String c : commandes){
			if(c.equals(getCommande())) return i;
			i++;
		}
		return -1;
  }
  private static void help(){
  		System.out.println("ImportCDM ");
	}
  	
  private  static String getArgName(String _arg){
		String name="";
		if(_arg.startsWith("--")) {
			String arg=_arg.substring(2);
			try {
				name=arg.split("=")[0];
			} catch(Exception e) {
				
			}
		}
		return name;
	}
	private  static String getArgValue(String _arg){
		String value="";
		if(_arg.startsWith("--")) {
			String arg=_arg;
			try {
				value=arg.split("=")[1];
			} catch(Exception e) {
				
			}
		}
		return value;
	}
  private static int getIndex(String s){
		int i=0;
		for(String c : arguments){
			if(c.equals(s)) return i;
			i++;
		}
		return -1;
	}
  public  static ImportCDM getImportCDM(String[] args){
	  	ImportCDM importCDM = new ImportCDM();
		for(String _arg:args){
			String arg=getArgName(_arg);
			int argi=getIndex(arg);
			String value=getArgValue(_arg);
			System.out.println(_arg+" "+arg+"("+argi+")="+value);
			switch (argi) {
				case 0 :  help(); System.exit(0);
				case 1 : importCDM.setUid(value); break;
				case 2 : importCDM.setPassword(value); break;
				case 3 : importCDM.setAnnee(value); break;
				case 4 : importCDM.setUrl(value); break;
				case 5 : importCDM.setSource(value); break;
				case 6 : importCDM.setXsl(value); break;
				case 7 : importCDM.setCommande(value); break;
				case 8 : importCDM.setOutputDir(value); break;
				default :  break;
			}	
		}
		return importCDM;
	}
	



}
