
 
package fr.lille1.util;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;

import java.util.*;

public class XslTransformer {
	private static Map CACHE = new Hashtable(); // map of Templates objects

	private static Transformer noTransformer;

	public static Transformer getTransformer(String xslPath)
			throws TransformerConfigurationException {
		TemplateWrapper stylesheet = (TemplateWrapper) CACHE.get(xslPath);
		if (stylesheet == null || stylesheet.isStale()) {
			TransformerFactory factory = TransformerFactory.newInstance();

			Templates template = factory
					.newTemplates(new StreamSource(xslPath));

			stylesheet = new TemplateWrapper(template, xslPath);
			CACHE.put(xslPath, stylesheet);
		}
		Transformer t = stylesheet.getStylesheet().newTransformer();
		t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		return t;
	}

	public static Transformer getTransformer()
			throws TransformerConfigurationException {
		if (noTransformer == null) {
			noTransformer = TransformerFactory.newInstance().newTransformer();
			// noTransformer.setOutputProperty(OutputKeys.ENCODING,
			// "ISO-8859-1");
			
		}
		return noTransformer;
	}

}
