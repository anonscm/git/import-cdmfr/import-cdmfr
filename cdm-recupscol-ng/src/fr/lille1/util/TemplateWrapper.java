/**
 * Copyright  2004 ESUP-portail consortium
 * Authors : rachid.siyoucef@univ-lille1.fr
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation augmented according to the
 * possibility of using it with programs developped under 
 * the Apache licence published by the Apache Software Foundation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * See the license terms site for more details : 
 * http://www.esup-portail.org/license.html
 */

/**
 * 
 * Description : <br/>
 * 
 * @version $Id : TemplateWrapper.java, V1.0, 15 décembre 2005<br/>
 * Copyright (c) 2004 Esup Portail (www.esup-portail.org) <br/>
 * Classe(s) : TemplateWrapper <br/>
 * @author Rachid Si Youcef <br/>
 */

package fr.lille1.util ;

import java.io.*;
import javax.xml.transform.*;
public class TemplateWrapper
{
    private Templates stylesheet; // the compiled stylesheet
    private File xslFile;         // represents the stylesheet source
    long timestamp;       // last compile time
        
    public TemplateWrapper(Templates aStylesheet, String xslFileName)
    {
        stylesheet = aStylesheet;
        xslFile    = new File(xslFileName);
        timestamp  = xslFile.lastModified();
    }
        
    public boolean isStale()
    {
        return xslFile.lastModified() != timestamp;
    }
        
    public Templates getStylesheet()
    {
        return stylesheet;
    }       
}
