<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  	<xsl:param name="debug" select="'n'"/>
	<xsl:output method="xml"/>
  	<xsl:key name="person" match="program/contacts/refPerson" use="@ref" />
  	<xsl:key name="lac" match="lac" use="@parcours" />
  
	
	<!-- variables -->
  	<xsl:variable name="diplomes" select="document('../cdm-content/diplomes.xml')/diplomes"/>
  	<xsl:variable name="domaines" select="document('../cdm-content/domaines.xml')/domaines"/>	
	<xsl:variable name="courses" select="document('../cdm-content/courses.xml')/courses"/>

	<!-- on rentre par ici -->
	<xsl:template match="/">
		<!-- on crée un fichier de travail qui est /CDM-complet -->
		<CDM-complet>
			<xsl:apply-templates select="/formations/*"/>
			<xsl:apply-templates select="$courses/*"/>
		</CDM-complet>
	</xsl:template>
	
	<xsl:template match="degree">
		<xsl:variable name="code" select="@degree"/>
		<degree degree="{$diplomes/diplome[@scol=$code]/@cdm}"/>
	</xsl:template>
	
	<!--<xsl:template match="codeDomain">
		<xsl:variable name="code" select="."/>
		<codeDomain><xsl:value-of select="$domaines/domaine[@scol=$code]/@cdm"/></codeDomain>
	</xsl:template>-->

	<xsl:template match="programCode">
		<xsl:variable name="code" select="./codeDomain"/>
    		<xsl:call-template name="explode">
      			<xsl:with-param name="str" select="$code" />
    		</xsl:call-template>
	</xsl:template>

	<!-- string processing through recursion -->
	<xsl:template name="explode">
  		<xsl:param name="str"/>

  		<xsl:variable name="temp" select="concat($str, ',')" />
  		<xsl:variable name="head" select="substring-before($temp, ',')" />
  		<xsl:variable name="tail" select="substring-after($temp, ',')" />

  		<xsl:if test="$head != ''">
    			<programCode>
    				<codeDomain><xsl:value-of select="$domaines/domaine[@scol=$head]/@cdm" /></codeDomain>
    			</programCode>
    			<xsl:call-template name="explode">
      				<xsl:with-param name="str" select="$tail" />
    			</xsl:call-template>
  		</xsl:if>
	</xsl:template>
	
	<xsl:template match="refProgram">
		<xsl:variable name="id" select="../../@id"/>
		<xsl:apply-templates select="/formations/program[programStructure/refProgram/@ref=$id]" mode="ref"/>
	</xsl:template>

	<!--
	<xsl:template match="refCourse">
    		<xsl:variable name="ref" select="@ref"/>
                	<xsl:if test="/courses[@id=$ref]">
      				<xsl:copy>
        				<xsl:apply-templates select="@*|*|text()"/>
      				</xsl:copy>
    			</xsl:if>
    			<xsl:if test="not(/CDM-gisele/courses[@id=$ref])"><ERREUR_COURSE ref="{$ref}"/></xsl:if>
        </xsl:template>
	-->	

	<xsl:template match="refCourse">
		<xsl:variable name="id" select="../../@id"/>
		<xsl:apply-templates select="$courses/course[courseStructure/refCourse/@ref=$id]" mode="ref"/>
	</xsl:template>

	<xsl:template match="program" mode="ref">
		<!--<refProgram ref="{@id}" name="{programName/text}"/>-->
		<refProgram ref="{@id}" idRef="{@id}"/>
	</xsl:template>

	<xsl:template match="course" mode="ref">
		<refCourse ref="{@id}" idRef="{@id}"/>
	</xsl:template>	

  	<xsl:template match="*|@*">
		<xsl:copy>
			<xsl:apply-templates select="@*|*|text()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
