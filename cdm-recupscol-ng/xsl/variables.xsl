<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"/>
  <xsl:variable name="id-etablissement" select="'FR_RNE_0593559Y'"/>
  <xsl:variable name="prefixe-id-program"><xsl:value-of select="$id-etablissement"/>_PR_</xsl:variable>
  <xsl:variable name="prefixe-id-person"><xsl:value-of select="$id-etablissement"/>_PE_</xsl:variable>
  <xsl:variable name="prefixe-id-course"><xsl:value-of select="$id-etablissement"/>_CO_</xsl:variable>


</xsl:stylesheet>
