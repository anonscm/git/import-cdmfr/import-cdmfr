<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wu="xalan://fr.lille1.util.WordUtils" exclude-result-prefixes="wu">
	<xsl:output method="xml" encoding="UTF-8"/>
  <xsl:key name="program" match="program" use="@id" />
  <xsl:param name="code"/>
  <xsl:variable name="orgUnit" select="document('../cdm-content/orgUnit.xml')/CDM"/>
	<xsl:variable name="catalogue-gisele" select="document('../cdm-content/catalogue-gisele.xml')/CDM-gisele"/>
  <xsl:include href="variables.xsl"/>
	<xsl:template match="/">
    
      <CDM>
        <xsl:if test="$code!=''">
          <properties/>
          <xsl:apply-templates select="$orgUnit/orgUnit"/>
          <xsl:apply-templates select="/CDM-complet/program[@id=$code]"/>
        </xsl:if>
      </CDM>
    
	</xsl:template>
  <xsl:template match="program">
    <xsl:copy>
      <xsl:apply-templates select="@*|*"/>
    </xsl:copy>
    <xsl:apply-templates select="programStructure/refProgram[not(@role) or @role!='semestre']" mode="creation"/>
    <xsl:apply-templates select="contacts/refPerson" mode="creation"/>
    <xsl:apply-templates select="programStructure/refCourse" mode="creation"/>
    <xsl:apply-templates select="subProgram/programStructure//refCourse" mode="creation"/>
  </xsl:template>
  
  <xsl:template match="refProgram" mode="creation">
    <xsl:variable name="ref" select="@ref"/>
    <xsl:apply-templates select="/CDM-complet/program[@id=$ref]"/>
  </xsl:template>
  <xsl:template match="refPerson" mode="creation">
    <xsl:variable name="ref" select="@ref"/>
    <xsl:apply-templates select="/CDM-complet/person[@id=$ref]"/>
  </xsl:template>
  <xsl:template match="refCourse" mode="creation">
    <xsl:variable name="ref" select="@ref"/>
    <xsl:apply-templates select="/CDM-complet/course[@id=$ref]"/>
  </xsl:template>
  <!--xsl:template match="program/teachingPlace|program/formOfTeaching|program/programDuration|program/universalAdjustment|program/regulations|program/studyAboard"/>



  <xsl:template match="program/@*">
    <xsl:param name="fiche"/>
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-program"/><xsl:value-of select="$fiche/@id"/></xsl:attribute>
	</xsl:template>

  <xsl:template match="subProgram/@*">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-program"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="refProgram/@ref|refProgram/@idRef">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-program"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="course/@*">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-course"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="refCourse/@ref|refCourse/@idRef">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-course"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="person/@*">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-person"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="refPerson/@ref|refPerson/@idRef">
		<xsl:attribute name="{name()}"><xsl:value-of select="$prefixe-id-person"/><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

  <xsl:template match="programID">
    <xsl:param name="fiche"/>
		<programID><xsl:value-of select="$prefixe-id-program"/><xsl:value-of select="$fiche/@id"/></programID>
	</xsl:template>

  <xsl:template match="courseID">
    <xsl:param name="fiche"/>
		<courseID><xsl:value-of select="$prefixe-id-course"/><xsl:value-of select="."/></courseID>
	</xsl:template>

  <xsl:template match="program/programName/text">
    <xsl:param name="fiche"/>
		<xsl:copy><xsl:value-of select="$fiche/programName/text"/></xsl:copy>
	</xsl:template>
  <xsl:template match="program/programCode">
    <xsl:param name="fiche"/>
		<xsl:apply-templates select="$fiche/programCode"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/programCode"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/webLink">
    <xsl:param name="fiche"/>
    <xsl:variable name="role" select="@role"/>
		<xsl:apply-templates select="$fiche/webLink[@role=$role]"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/webLink[@role=$role]"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/programDescription">
    <xsl:param name="fiche"/>
    <programDescription nature="{$fiche/programDescription/@nature}">
      <xsl:apply-templates select="$fiche/programDescription/*"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
      <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/programDescription/*"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    </programDescription>
	</xsl:template>
  <xsl:template match="program/qualification/qualificationDescription">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/qualification/qualificationDescription"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/qualification/qualificationDescription"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/qualification/degree">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/qualification/degree"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/qualification/profession">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/qualification/profession"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/qualification/profession"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/qualification/studyQualification">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/qualification/studyQualification"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/qualification/studyQualification"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/level">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/level"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/learningObjectives">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/learningObjectives"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/learningObjectives"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/recommandedPrerequisites">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/recommandedPrerequisites"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/recommandedPrerequisites"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/formalPrerequisites">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/formalPrerequisites"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/formalPrerequisites"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
  <xsl:template match="program/programStructure">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/programStructure"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/programStructure"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
	</xsl:template>
   <xsl:template match="program/contacts">
    <xsl:param name="fiche"/>
    <contacts>
      <xsl:apply-templates select="$fiche/contacts/refPerson"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
      <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/contacts/refPerson"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    </contacts>
	</xsl:template>
  <xsl:template match="program/subProgram">
    <xsl:param name="fiche"/>
    <xsl:apply-templates select="$fiche/programStructure/refProgram" mode="creation-subProgram"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    <xsl:apply-templates select="$catalogue-gisele/program[@id=$fiche/@id]/programStructure/refProgram"  mode="creation-subProgram"><xsl:with-param name="fiche" select="$fiche"/></xsl:apply-templates>
    
	</xsl:template-->
  <xsl:template match="*|@*">
    <xsl:param name="fiche"/>
		<xsl:copy>
			<xsl:apply-templates select="@*|*|text()">
        <xsl:with-param name="fiche" select="$fiche"/>
      </xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
